<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Business;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Policies\BasePolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class BusinessPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Business  $business
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Business $business)
    {
        //
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
            return true;
        else
            return $user->id === (int)$business->created_by;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Business  $business
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Business $business)
    {
        //
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
            return true;
        else
            return $user->id === (int)$business->created_by;
    }
}
